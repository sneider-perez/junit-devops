package com.example.demo;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class UtilitiesTest {

    @Test
    void testPrimeNumber() {
        assertTrue(Utilities.isPrime(17));
        assertTrue(Utilities.isPrime(5));
        assertTrue(Utilities.isPrime(1));
        assertFalse(Utilities.isPrime(9));
    }

    @Test
    void testPairNumber() {
        assertFalse(Utilities.isPair(19));
        assertTrue(Utilities.isPair(12));
        assertFalse(Utilities.isPair(5));
        assertFalse(Utilities.isPair(-1));
    }

    @Test
    void testReverseString() {
        String original = "Hola";
        String reverse = "aloH";
        assertEquals(reverse, Utilities.reverseString(original));
    }

    @Test
    void testSortArray() {
        int[] array ={6,3,4,8,9,1,2,7};
        int[] sort = {1,2,3,4,6,7,8,9};

        assertArrayEquals(sort, Utilities.sortArray(array));
    }

    @Test
    void testNotNull() {
        Map<String, String> map = new HashMap<>();
        map.put("one", "uno");
        map.put("two", "dos");
        map.put("three", "tres");
        assertNotNull(Utilities.getItem(map,"three"));
        assertNotNull(Utilities.getItem(map,"one"));
    }

    @Test
    void testSame(){
        Student std1=new Student(6,"Pedro", "Ruiz", "antony@gmail.com");
        Student std2 = new Student(6,"Pedro", "Ruiz", "antony@gmail.com");

        assertNotSame(std1, std2);
        std2=std1;
        assertSame(std1,std2);
    }

}
