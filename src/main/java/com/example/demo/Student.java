package com.example.demo;

public class Student {

    private int code;
    private String name;
    private String surname;
    private String mail;

    public Student() {
    }

    public Student(int code, String name, String surname, String mail) {
        this.code = code;
        this.name = name;
        this.surname = surname;
        this.mail = mail;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

}

