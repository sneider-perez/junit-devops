package com.example.demo;

import java.util.List;
import java.util.Map;

public class Utilities {

    public static int[] sortArray(int[] array){
        for (int x = 0; x < array.length; x++) {
            for (int i = 0; i < array.length-x-1; i++) {
                if(array[i] > array[i+1]){
                    int tmp = array[i+1];
                    array[i+1] = array[i];
                    array[i] = tmp;
                }
            }
        }
        return array;
    }

    public static String reverseString(String str){
        StringBuilder newStr = new StringBuilder();
        int len = str.length();

        for (int i = len-1; i>=0; i--){
            newStr.append(str.charAt(i));
        }
        return newStr.toString();
    }

    public static boolean isPrime(int number){
        for (int i =2; i<number;i++){
            if(number%i==0){
                return false;
            }
        }
        return true;
    }

    public static boolean isPair(int number){
        return number%2==0;
    }

    public static String getItem( Map<String, String> map, String key){
        return map.get(key);
    }


}
